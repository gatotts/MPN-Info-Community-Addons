#ifndef DATABASEUPDATE_H
#define DATABASEUPDATE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QCryptographicHash>
#include <QDebug>

#include <coreengine.h>
#include <interface.h>
#include <runnable.h>

class CRunnable : public Runnable
{
public:
    CRunnable(CoreEngine *_engine) : engine(_engine) {}

    void run() {
        DatabasePtr db = engine->database();
        db->exec("SELECT `value` FROM `info` WHERE `key`='db.version'");

        QString version;
        if (db->next())
            version = db->value(0).toString();

        if (version.isEmpty()) {
            db->exec("CREATE TABLE `info` ( `key` varchar(24) PRIMARY KEY NOT NULL, `value` text NOT NULL )");
            db->exec("CREATE TABLE `settings` ( `key` varchar(128) PRIMARY KEY NOT NULL, `value` text NOT NULL )");
            db->exec("CREATE TABLE `pegawai` ( `kantor` varchar(3) NOT NULL, `nip` varchar(9) NOT NULL, `nip2` text NULL, `nama` text NOT NULL, `pangkat` integer NOT NULL, `seksi` integer NULL, `jabatan` integer NOT NULL, `tahun` integer NOT NULL, `plh` varchar(9) NULL )");
            db->exec("CREATE TABLE `mpn` ( `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nama` text NULL, `kdmap` varchar(6) NULL, `kdbayar` varchar(3) NULL, `masa` integer NULL, `tahun` integer NULL, `tanggalbayar` integer NOT NULL, `bulanbayar` integer NOT NULL, `tahunbayar` integer NOT NULL, `datebayar` date NOT NULL, `nominal` double NULL, `ntpn` varchar(16) NULL, `bank` text NULL, `nosk` varchar(15) NULL, `nospm` varchar(18) NULL, `tipe` integer NOT NULL, `source` integer NOT NULL, `extra` text NULL, `billing` varchar(15) NULL, `nop` varchar(18) NULL, `pembuat` text NULL, `ket` text NULL )");
            db->exec("CREATE TABLE `wp` ( `admin` varchar(3) NOT NULL, `npwp` text NOT NULL, `kpp` text NOT NULL, `cabang` text NOT NULL, `nama` text NULL, `alamat` text NULL, `kelurahan` text NULL, `kecamatan` text NULL, `kota` text NULL, `propinsi` text NULL, `jenis` text NULL, `bentukhukum` text NULL, `status` text NULL, `klu` text NULL, `sektor` text NULL, `tanggaldaftar` date NULL, `tanggalpkp` date NULL, `tanggalpkpcabut` date NULL, `nik` text NULL, `telp` text NULL, `masterfile` integer NULL, `wpbesar` integer NULL, `bayar` integer NULL, `lapor` integer NULL, `eksten` integer NULL, `assign` integer NULL, `nippj` text NULL, `nipar` text NULL, `nipeks` text NULL, `nipjs` text NULL )");
            db->exec("CREATE TABLE `renpen` ( `kpp` varchar(3) NOT NULL, `nip` varchar(9) NOT NULL, `kdmap` text NOT NULL, `bulan` integer NOT NULL, `tahun` integer NOT NULL, `target` double NOT NULL )");
            db->exec("CREATE TABLE `masterfile` ( `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nama` text NULL, `alamat` text NULL, `kelurahan` text NULL, `kecamatan` text NULL, `kota` text NULL, `propinsi` text NULL, `jenis` text NULL, `bentukhukum` text NULL, `status` text NULL, `klu` text NULL, `tanggaldaftar` date NULL, `tanggalpkp` date NULL, `tanggalpkpcabut` date NULL, `nik` text NULL, `telp` text NULL, `nipar` text NULL, `nipeks` text NULL, `nipjs` text NULL )");
            db->exec("CREATE TABLE `masterspt` ( `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nomor` text NULL, `jenis` text NULL, `masa` integer NULL, `tahun` integer NULL, `pembetulan` integer NULL, `tanggalspt` date NULL, `matauang` text NULL, `status` text NULL, `nominal` double NULL, `caralapor` text NULL, `nip` text NULL, `tanggalterima` DATE NULL, `tanggalbayar` DATE NULL, `media` text NULL )");
            db->exec("CREATE TABLE `seksi` ( `id` integer NOT NULL, `kantor` varchar(3) NOT NULL, `tipe` integer  NOT NULL, `nama` text NOT NULL, `kode` varchar(2) NULL, `telp` text NULL )");
            db->exec("CREATE TABLE `klu` ( `kode` text NOT NULL, `nama` text NOT NULL, `sektor` text NOT NULL )");
            db->exec("CREATE TABLE `kantor` ( `kanwil` varchar(3) NOT NULL, `kpp` varchar(3) NOT NULL, `nama` text NOT NULL )");
            db->exec("CREATE TABLE `assignpj` ( `admin` varchar(3) NOT NULL,`npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nip` varchar(9) NOT NULL )");
            db->exec("CREATE TABLE `assignjs` ( `admin` varchar(3) NOT NULL,`npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nip` varchar(9) NOT NULL )");
            db->exec("CREATE TABLE `assignklu` ( `admin` varchar(3) NOT NULL,`npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `klu` varchar(5) NOT NULL )");
            db->exec("CREATE TABLE `map` ( `kdmap` varchar(6) NOT NULL, `kdbayar` varchar(9) NOT NULL, `sektor` integer NOT NULL, `uraian` text NOT NULL )");
            db->exec("CREATE TABLE `jatuhtempo` ( `bulan` integer NOT NULL, `tahun` integer NOT NULL, `potput` date NULL, `pph` date NULL, `ppn` date NULL, `pphop` date NULL, `pphbdn` date NULL )");
            db->exec("CREATE TABLE `maxlapor` ( `bulan` integer NOT NULL, `tahun` integer NOT NULL, `pph` date NULL, `ppn` date NULL, `pphop` date NULL, `pphbdn` date NULL )");
            db->exec("CREATE TABLE `wpbesar` ( `admin` varchar(3) NOT NULL, `npwp` text NOT NULL, `kpp` text NOT NULL, `cabang` text NOT NULL )");
            db->exec("CREATE TABLE `wpfavorit` ( `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nip` varchar(9) NOT NULL, `list` text NOT NULL )");
            db->exec("CREATE TABLE `wpfavoritlist` ( `nip` varchar(9) NOT NULL, `nama` text NOT NULL )");
            db->exec("CREATE TABLE `catatan` ( `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `catatan` text NULL )");
            db->exec("CREATE TABLE `appportal_spttahunan` ( `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `jenis` text NOT NULL, `tahun` integer NOT NULL, `omzet` double NULL, `phlain` double NULL, `phfinal` double NULL, `phnotax` double NULL )");
            db->exec("CREATE TABLE `appportal_sptppn` ( `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `jenis` text NOT NULL, `tahun` integer NOT NULL, `jumlahspt` integer NULL, `ekspor` double NULL, `dppdn` double NULL, `jumlahpk` integer NULL, `dpppk` double NULL )");
            db->exec("CREATE TABLE `appportal_sptppn_detail` ( `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `jenis` text NOT NULL, `masa` integer NOT NULL, `tahun` integer NOT NULL, `ekspor` double NULL, `dppdn` double NULL )");
            db->exec("CREATE TABLE `appportal_eq_omzetppn_log` ( `admin` text NOT NULL, `tahun` integer NOT NULL, `lastupdate` date NULL  )");
            db->exec("CREATE TABLE `appportal_pkpm` ( `npwp` varchar(15) NOT NULL, `tipe` integer NOT NULL, `tahun` integer NOT NULL, `nofaktur` text NULL, `tanggalfaktur` date NULL, `masasendiri` text NULL, `masalawan` text NULL, `npwplawan` varchar(15) NOT NULL, `namalawan` text NULL, `nominalsendiri` double NULL, `nominallawan` double NULL, `suspect` integer NULL )");
            db->exec("CREATE TABLE `appportal_pkpm_log` ( `npwp` varchar(15) NOT NULL, `tipe` integer NOT NULL, `tahun` integer NOT NULL, `lastupdate` date NULL )");
            db->exec("CREATE TABLE `appportal_bupot` ( `npwp` varchar(15) NOT NULL, `npwplawan` varchar(15) NOT NULL, `namalawan` text NULL, `tahun` integer NOT NULL, `klu` text NOT NULL, `lembar` integer NOT NULL, `objek` double NULL, `jenis` text NULL )");
            db->exec("CREATE TABLE `appportal_bupot_log` ( `npwp` varchar(15) NOT NULL, `lastupdate` date NULL )");
            db->exec("CREATE TABLE `appportal_nsfp_sebelum` ( `admin` varchar(3) NOT NULL, `tahun` integer NOT NULL, `npwp` varchar(15) NOT NULL, `nama` text NOT NULL, `masa` integer NOT NULL, `pembetulan` integer NOT NULL, `nofaktur` text NOT NULL, `tanggalfaktur` date NOT NULL, `dpp` double NOT NULL, `ppn` double NOT NULL, `nopemberitahuan` text NOT NULL, `tanggalpemberitahuan` text NOT NULL, `nsfpawal` text NOT NULL, `nsfpakhir` text NOT NULL )");
            db->exec("CREATE TABLE `appportal_nsfp_bukan` ( `admin` varchar(3) NOT NULL, `tahun` integer NOT NULL, `npwp` varchar(15) NOT NULL, `nama` text NOT NULL, `masa` integer NOT NULL, `pembetulan` integer NOT NULL, `nofaktur` text NOT NULL, `tanggalfaktur` date NOT NULL, `dpp` double NOT NULL, `ppn` double NOT NULL )");
            db->exec("CREATE TABLE `appportal_nsfp_log` ( `admin` text NOT NULL, `tahun` integer NOT NULL, `lastupdate` date NULL )");
            db->exec("CREATE TABLE `nothitstppembayaran` ( `nothitstp` integer NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `kdmap` text NOT NULL, `kdbayar` text NOT NULL, `masa` integer NOT NULL, `tahun` integer NOT NULL, `tanggalbayar` date NOT NULL, `ntpn` text NOT NULL, `nominal` double NOT NULL, `bulanterlambat` integer NOT NULL, `bunga` double NOT NULL, `dasar` integer NOT NULL )");
            db->exec("CREATE TABLE `nothitstppelaporan` ( `nothitstp` integer NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `jenis` text NOT NULL, `masa` integer NOT NULL, `tahun` integer NOT NULL, `tanggalterima` date NULL, `denda` double NOT NULL, `tipe` integer NOT NULL )");
            db->exec("CREATE TABLE `pbk` ( `admin` text NOT NULL, `tahun` integer NOT NULL, `nopbk` text NOT NULL, `tanggaldoc` date NOT NULL, `tanggalberlaku` date NOT NULL, `nominal` double NOT NULL, `currency` text NOT NULL, `tipe` text NOT NULL, `status` text NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nama` text NOT NULL, `kdmap` varchar(6) NOT NULL, `kdbayar` varchar(3) NOT NULL, `masapajak` integer NOT NULL, `tahunpajak` integer NOT NULL, `npwp2` varchar(9) NOT NULL, `kpp2` varchar(3) NOT NULL, `cabang2` varchar(3) NOT NULL, `nama2` text NOT NULL, `kdmap2` varchar(6) NOT NULL, `kdbayar2` varchar(3) NOT NULL, `masapajak2` integer NOT NULL, `tahunpajak2` integer NOT NULL, `ntpn` TEXT NULL  )");
            db->exec("CREATE TABLE `dth_satker` (`kdsatker` VARCHAR(6) NOT NULL, `nama` VARCHAR(128), `npwp` VARCHAR(16))");
            db->exec("CREATE TABLE `dth_sp2d` (`tahun` INTEGER(4) NOT NULL, `bulan` INTEGER(2) NOT NULL, `kdsatker` VARCHAR(6) NOT NULL, `kdpemda` VARCHAR(4) NOT NULL, `kdurusan` VARCHAR(2) NOT NULL, `urusan` VARCHAR(128) NOT NULL, `kdklpurusan` VARCHAR(2) NOT NULL, `klpurusan` VARCHAR(128) NOT NULL, `kdorganisasi` VARCHAR(2) NOT NULL, `organisasi` VARCHAR(128) NOT NULL, `sp2d` VARCHAR(64) NOT NULL, `spm` VARCHAR(64) NOT NULL, `jenis` INTEGER(2) NOT NULL, `tanggal` VARCHAR(45) NOT NULL, `nilai` DOUBLE NOT NULL, `npwpbud` VARCHAR(16), `npwpbend` VARCHAR(16), `npwprekan` VARCHAR(16), `keterangan` TEXT, `pejabat` VARCHAR(128))");
            db->exec("CREATE TABLE `dth_belanja` (`tahun` INTEGER(4) NOT NULL, `bulan` INTEGER(2) NOT NULL, `kdsatker` VARCHAR(6) NOT NULL, `kdurusan` VARCHAR(2) NOT NULL, `kdklpurusan` VARCHAR(2) NOT NULL, `kdorganisasi` VARCHAR(2) NOT NULL, `sp2d` VARCHAR(64) NOT NULL, `kdakun` INTEGER(2), `akun` VARCHAR(128), `kdklpakun` INTEGER(2), `klpakun` VARCHAR(128), `kdjenisakun` INTEGER(2), `jenisakun` VARCHAR(128), `kdobjekakun` INTEGER(2), `objekakun` VARCHAR(128), `kdrincianobjekakun` VARCHAR(2), `rincianobjekakun` VARCHAR(128), `nilai` DOUBLE NOT NULL)");
            db->exec("CREATE TABLE `dth_potongan` (`tahun` INTEGER(4) NOT NULL, `bulan` INTEGER(2) NOT NULL, `kdsatker` VARCHAR(6) NOT NULL, `kdurusan` VARCHAR(2) NOT NULL, `kdklpurusan` VARCHAR(2) NOT NULL, `kdorganisasi` VARCHAR(2) NOT NULL, `sp2d` VARCHAR(64) NOT NULL, `kdpajak` VARCHAR(6) NOT NULL, `nilai` DOUBLE NOT NULL)");
            db->exec("CREATE TABLE `spt` ( `npwp` VARCHAR(9) NOT NULL, `kpp` VARCHAR(3) NOT NULL, `cabang` VARCHAR(3) NOT NULL, `masa` INTEGER NOT NULL, `tahun` INTEGER NOT NULL, `pembetulan` INTEGER NOT NULL, `jenis` varchar(32) NOT NULL, `tanggal` DATE NOT NULL, `data` TEXT NOT NULL )");

            if (db->driverName() == "QMYSQL") {
                db->exec("CREATE TABLE `users` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `username` text NOT NULL, `password` text NOT NULL, `fullname` text NOT NULL, `group` integer NOT NULL )");
                db->exec("CREATE TABLE `spmkp` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `kdmap` varchar(6) NOT NULL, `bulan` integer NOT NULL, `tahun` integer NOT NULL, `tanggal` DATE NULL, `nominal` double NOT NULL )");
                db->exec("CREATE TABLE `spmpp` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `kdmap` varchar(6) NOT NULL, `bulan` integer NOT NULL, `tahun` integer NOT NULL, `tanggal` DATE NULL, `nominal` double NOT NULL )");
                db->exec("CREATE TABLE `notify` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `nip` varchar(9) NOT NULL, `tipe` integer NOT NULL, `args` text NULL )");
                db->exec("CREATE TABLE `lokasi` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `title` text  NOT NULL, `desc` text NULL, `lat` double  NOT NULL, `lng` double  NOT NULL, `tanggal` date NOT NULL )");
                db->exec("CREATE TABLE `document` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `group` integer NOT NULL, `nomor` text NOT NULL, `hal` text NOT NULL, `filename` text NOT NULL, `catatan` text NULL )");
                db->exec("CREATE TABLE `documentgroup` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nama` text NOT NULL )");
                db->exec("CREATE TABLE `nothitstp` ( `id` integer PRIMARY KEY AUTO_INCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `jenis` text NOT NULL, `masa` integer NOT NULL, `masa2` integer NOT NULL, `tahun` integer NOT NULL, `denda` double NULL, `bunga` double NULL, `bungapembetulan` double NULL, `tanggalterbit` date NOT NULL, `nip` text NOT NULL )");
                db->exec("CREATE TABLE `sp2dk` ( `id` INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL, `npwp` VARCHAR(9) NOT NULL, `kpp` VARCHAR(3) NOT NULL, `cabang` VARCHAR(3) NOT NULL, `masastart` INTEGER NOT NULL, `masaend` INTEGER NOT NULL, `tahun` INTEGER NOT NULL, `tipe` INTEGER NOT NULL, `tanggal` DATE NOT NULL, `data` TEXT NOT NULL )");
            }
            else if (db->driverName() == "QSQLITE" || db->driverName() == "QSQLCIPHER") {
                db->exec("CREATE TABLE `users` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `username` text NOT NULL, `password` text NOT NULL, `fullname` text NOT NULL, `group` integer NOT NULL )");
                db->exec("CREATE TABLE `spmkp` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `kdmap` varchar(6) NOT NULL, `bulan` integer NOT NULL, `tahun` integer NOT NULL, `tanggal` DATE NULL, `nominal` double NOT NULL )");
                db->exec("CREATE TABLE `spmpp` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `admin` varchar(3) NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `kdmap` varchar(6) NOT NULL, `bulan` integer NOT NULL, `tahun` integer NOT NULL, `tanggal` DATE NULL, `nominal` double NOT NULL )");
                db->exec("CREATE TABLE `notify` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `nip` varchar(9) NOT NULL, `tipe` integer NOT NULL, `args` text NULL )");
                db->exec("CREATE TABLE `lokasi` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `title` text  NOT NULL, `desc` text NULL, `lat` double  NOT NULL, `lng` double  NOT NULL, `tanggal` date NOT NULL )");
                db->exec("CREATE TABLE `document` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `group` integer NOT NULL, `nomor` text NOT NULL, `hal` text NOT NULL, `filename` text NOT NULL, `catatan` text NULL )");
                db->exec("CREATE TABLE `documentgroup` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `nama` text NOT NULL )");
                db->exec("CREATE TABLE `nothitstp` ( `id` integer PRIMARY KEY AUTOINCREMENT NOT NULL, `npwp` varchar(9) NOT NULL, `kpp` varchar(3) NOT NULL, `cabang` varchar(3) NOT NULL, `jenis` text NOT NULL, `masa` integer NOT NULL, `masa2` integer NOT NULL, `tahun` integer NOT NULL, `denda` double NULL, `bunga` double NULL, `bungapembetulan` double NULL, `tanggalterbit` date NOT NULL, `nip` text NOT NULL )");
            }

            db->exec("INSERT INTO `users`(`username`, `password`, `fullname`, `group`) VALUES('admin', 'admin', 'Administrator', 0)");
            db->exec("INSERT INTO `info` VALUES('db.version', '4.4')");
            version = "4.4";
        }

        /*
        if (version == "4.4") {
            QHash<QString, QString> authHash;
            db->exec("SELECT `username`, `password` FROM `users`");
            while (db->next()) {
                QString username = db->value(0).toString();
                QString password = db->value(1).toString();

                authHash[username] = QCryptographicHash::hash(QString(username + ":" + password).toUtf8(), QCryptographicHash::Sha1).toHex();
            }

            QHashIterator<QString, QString> iterator(authHash);
            while (iterator.hasNext()) {
                iterator.next();
                db->exec("UPDATE `users` SET `password`=? WHERE `username`=?");
                db->addBindValue(iterator.value());
                db->addBindValue(iterator.key());
                db->exec();
            }

            db->exec("UPDATE `info` SET `value`='5.0' WHERE `key`='db.version'");
            version = "5.0";
        }
        */
    }

    CoreEngine *engine;
};

class DatabaseUpdate : public Process
{
public:
    DatabaseUpdate(CoreEngine *engine) : mEngine(engine) {}

    void run() {
        ObjectPtr loadingDialog = mEngine->addOn("dialog_loading")->newObject();
        loadingDialog->exec("setMessage", "Updating Database");
        loadingDialog->exec("show");

        mEngine->runSync(new CRunnable(mEngine));

        loadingDialog->exec("setMessage", "Updating Kantor");
        mEngine->addOn("database_update_kantor")->newProcess()->run();

        loadingDialog->exec("setMessage", "Updating Kode MAP");
        mEngine->addOn("database_update_map")->newProcess()->run();

        loadingDialog->exec("setMessage", "Updating KLU");
        mEngine->addOn("database_update_klu")->newProcess()->run();

        loadingDialog->exec("setMessage", "Updating Jatuh Tempo");
        mEngine->addOn("database_update_jatuhtempo")->newProcess()->run();

        loadingDialog->exec("setMessage", "Updating Max Lapor");
        mEngine->addOn("database_update_maxlapor")->newProcess()->run();
    }

private:
    CoreEngine *mEngine;
};

#endif // DATABASEUPDATE_H
